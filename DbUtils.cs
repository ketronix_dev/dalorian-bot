using System.Data;
using System.Text;
using MySql.Data.MySqlClient;
using Tomlyn;

namespace TelegramBotExperiments;

public class DbUtils
{
    public static MySqlConnection GetDBConnection()
    {
        string host;
        string database;
        string username;
        string password;
        
        using (FileStream fstream = File.OpenRead("config.toml"))
        {
            byte[] buffer = new byte[fstream.Length];
            fstream.Read(buffer, 0, buffer.Length);
            string textFromFile = Encoding.Default.GetString(buffer);

            var model = Toml.ToModel(textFromFile);
            host = (string) model["addressDatabase"]!;
            database = (string) model["nameDatabase"]!;
            username = (string) model["nameUserDatabase"]!;
            password = (string) model["passwordUserDatabase"]!;
        }
        String connString = "Server=" + host + ";Database=" + database
                            + ";User Id=" + username + ";password=" + password;
        MySqlConnection conn = new MySqlConnection(connString);

        return conn;
    }
    public static void CreateTableOrNo(MySqlConnection conn)
    {
        conn.Open();
        try
        {
            string query = "CREATE TABLE `user` (`id` BIGINT NULL DEFAULT NULL , `karma` INT NULL DEFAULT NULL, `lastdate` DATETIME NULL DEFAULT NULL ) ENGINE = InnoDB;";
            MySqlCommand command = new MySqlCommand(query, conn);
            command.ExecuteNonQuery();
        }
        catch (MySqlException) { }
        conn.Close();
    }

    public static int GetKarma(MySqlConnection conn, long id)
    {
        string sql = String.Format("SELECT karma FROM user WHERE id = {0};", id);
        conn.Open();
        using var cmd = new MySqlCommand(sql, conn);
        using MySqlDataReader rdr = cmd.ExecuteReader();
        int karma = -1;
        if (rdr.Read())
        {
            karma = rdr.GetInt32(0);
            rdr.Close();
        }
        conn.Close();
        return karma;
    }

    public static List<UserData> GetAllKarma(MySqlConnection conn)
    {
        var listItems = new List<UserData>() { };
        string sql = "SELECT id, karma FROM `user`;";
        
        conn.Open();
        using var cmd = new MySqlCommand(sql, conn);
        using MySqlDataReader rdr = cmd.ExecuteReader();

        while (rdr.Read())
        {
            long id = rdr.GetInt64(0);
            int karma = rdr.GetInt32(1);
            /*Console.WriteLine("\nID: " + id + "\nKarma: " + karma);*/
            var item = new UserData() { Id = id, Karma = karma };
            listItems.Add(item);
        }
        rdr.Close();
        conn.Close();
        
        List<UserData> results = listItems.GroupBy(x => x.Id).Select(x => x.First()).ToList();
        var users = results.OrderByDescending(x => x.Karma).ToList();
        if (users.Count <= 10)
        {
            return users.GetRange(0, 9);
        }
        else
        {
            return users.GetRange(0, users.Count);   
        }
    }
    public static int GetDate(MySqlConnection conn, long id)
    {
        var listItems = new List<DateTime>() { };
        string sql = "SELECT lastdate FROM `user`;";
        
        conn.Open();
        using var cmd = new MySqlCommand(sql, conn);
        using MySqlDataReader rdr = cmd.ExecuteReader();
        
        while (rdr.Read())
        {
            DateTime date = rdr.GetDateTime(0);
            listItems.Add(date);
        }
        rdr.Close();
        conn.Close();
        if (listItems.Count == 0)
        {
            InsertUser(GetDBConnection(), id);
            return 15;
        }
        else
        {
            List<DateTime> results = listItems
                .OrderByDescending(x => x)
                .ToList();
            int returned = -(int)(results[0] - DateTime.Now).TotalSeconds;
            return returned;
        }
    }

    public static void AddedKarma(MySqlConnection conn, int currentKarma, long id)
    {
        conn.Open();
        string sql = String.Format("UPDATE user SET karma = {0}, lastdate = NOW() WHERE id = {1}", currentKarma + 1, id);
        using var command = new MySqlCommand(sql, conn);
        command.ExecuteNonQuery();
        conn.Close();
    }
    
    public static bool checkUser(long id)
    {
        var conn = GetDBConnection();
        MySqlCommand cmd = conn.CreateCommand();
        cmd.CommandText = "SELECT COUNT(*) FROM user WHERE id=@id";
        cmd.Parameters.AddWithValue("@id",id);
        try
        {
            conn.Open();                
        } catch (Exception ex) {
            conn.Close();
        }  
        MySqlDataReader reader = cmd.ExecuteReader();  
        if(reader.Read())return true;
        else return false;
    }

    public static void InsertUser(MySqlConnection conn, long id)
    {
        conn.Open();
        var sql = String.Format("INSERT INTO user VALUES({0}, 1, NOW());", id);
        using var command = new MySqlCommand(sql, conn);
        command.ExecuteNonQuery();
        conn.Close();
    }
}
