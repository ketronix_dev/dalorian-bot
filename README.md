# Dalorian-Bot

My telegram bot to disallow sending messages on behalf of the channel and karma of the user. Also [KirMozor](https://gitlab.com/KirMozor) wrote code for the bot to work with the database, and fixed some bugs

## License
For open source projects, the license is GNU GPLv3+.

