﻿using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using File = System.IO.File;
using System.Text;
using System.Text.RegularExpressions;
using MySql.Data.MySqlClient;
using Tomlyn;

namespace TelegramBotExperiments
{
    class Program
    {
        static ITelegramBotClient bot = new TelegramBotClient("");
        public static async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            // Некоторые действия
            if (update.Type == UpdateType.Message)
            {
                var message = update.Message;
                /*if (message.From.IsBot && message.From.Username != "quackduck_bot")
                {
                    await botClient.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                    return;
                }*/
                
                if (message.Text == "/mykarma" || message.Text == "/mykarma@" + bot.GetMeAsync().Result.Username)
                {
                    if (DbUtils.checkUser(message.From.Id))
                    {
                        MySqlConnection connCreateUser = DbUtils.GetDBConnection();
                        DbUtils.InsertUser(connCreateUser, message.From.Id);
                    }
                    MySqlConnection conn = DbUtils.GetDBConnection();
                    int karma = DbUtils.GetKarma(conn, message.From.Id);
                    if (karma != -1)
                    {
                        await botClient.SendTextMessageAsync(message.Chat.Id,
                            new string(String.Format("Твоя карма: {0}", karma)));
                    }
                }

                if (message.Text == "/topkarma" || message.Text == "/topkarma@" + bot.GetMeAsync().Result.Username)
                {
                    MySqlConnection conn = DbUtils.GetDBConnection();
                    List<UserData> karmaUsers = DbUtils.GetAllKarma(conn);
                    var users = karmaUsers.OrderByDescending(x => x.Karma).ToArray();
                    string topKarmaMessage = "";
                    if (karmaUsers.Count >= 10)
                    {
                        for (var i = 0; users.Count() >= 10; i++)
                        {
                            if (i + 1 <= users.Count())
                            {
                                string FirstName;
                                string? lastName;
                                try
                                { 
                                    FirstName = Regex.Replace(
                                        botClient.GetChatMemberAsync(message.Chat.Id, users[i].Id)
                                            .Result
                                            .User
                                            .FirstName, "<&>", string.Empty);
                                }
                                catch (ArgumentNullException e) { FirstName = ""; }

                                try
                                {
                                    lastName = Regex.Replace(botClient.GetChatMemberAsync(message.Chat.Id, users[i].Id)
                                        .Result
                                        .User
                                        .LastName, "<&>", string.Empty);
                                }
                                catch (ArgumentNullException) { lastName = ""; }
                                
                                var UserName = "";
                                if (lastName != null)
                                {
                                    UserName = FirstName + " " + lastName;
                                }
                                else
                                {
                                    UserName = FirstName;
                                }
                                topKarmaMessage += 
                                    $"{(i + 1).ToString()}: <a href=\"tg://user?id={users[i].Id}\">{UserName}</a> - {users[i].Karma}\n";
                                Console.WriteLine(topKarmaMessage);
                            }
                            else
                            {
                                break;
                            }
                        }

                        await botClient.SendTextMessageAsync(message.Chat.Id, topKarmaMessage, ParseMode.Html);
                    }
                    else
                    {
                        await botClient.SendTextMessageAsync(message.Chat.Id, "Маловато-то пользователей для ТОП-а", ParseMode.Html);
                    }
                }

                if (message.Text == "+" || message.Text == "Спасибо"|| message.Text == "Спс"|| message.Text == "Сяп"|| message.Text == "От души")
                {
                    if (message.ReplyToMessage != null)
                    {
                        if (message.ReplyToMessage.From.Id != botClient.GetMeAsync().Id)
                        {
                            if (message.ReplyToMessage.From.IsBot == false)
                            {
                                if (message.ReplyToMessage.From.Id != message.From.Id)
                                {
                                    MySqlConnection conn = DbUtils.GetDBConnection();
                                    int karma = DbUtils.GetKarma(conn, message.ReplyToMessage.From.Id);
                                    int fromLast = DbUtils.GetDate(conn, message.ReplyToMessage.From.Id);
                                    /*Console.WriteLine(fromLast);*/

                                    if (fromLast >= 15)
                                    {
                                        if (DbUtils.checkUser(message.ReplyToMessage.From.Id))
                                        {
                                            conn.Close();
                                            DbUtils.InsertUser(conn, message.ReplyToMessage.From.Id);
                                        }
                                        // (int)dateWithLastKarma.Seconds
                                        if (karma != -1)
                                        {
                                            DbUtils.AddedKarma(conn, karma, message.ReplyToMessage.From.Id);
                                        }

                                        await botClient.SendTextMessageAsync(
                                            message.Chat.Id, 
                                            $"+ 1 в карму <a href=\"tg://user?id={message.ReplyToMessage.From.Id}\">{message.ReplyToMessage.From.FirstName}</a>",
                                            ParseMode.Html
                                        );
                                    }
                                    else
                                    {
                                        await botClient.SendTextMessageAsync(message.Chat.Id, "Эй-эй, не так быстро. Подожди немного, подумай.");
                                    }
                                }
                                else
                                {
                                    await botClient.SendTextMessageAsync(message.Chat.Id, "Самолюбие хорошо в меру. Но тут ты маленько перегнул.");
                                }
                            }
                            else
                            {
                                await botClient.SendTextMessageAsync(message.Chat.Id, "Бот не обломится, ему и без кармы хорошо живется.");
                            }
                        }
                        else
                        {
                            await botClient.SendTextMessageAsync(message.Chat.Id, "Товарищ, мне эта ваша карма не нужна, это я тут решаю, кому ее повышать, а кого закрыть в подвале и...");
                        }
                    }
                    else
                    {
                        await botClient.SendTextMessageAsync(message.Chat.Id, "Так низзя. Нужно сначала ответить на сообщение.");
                    }
                }
            }
        }

        public static async Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            // Некоторые действия
            Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(exception.Message));
        }
        
        static void Main(string[] args)
        {
            if (!File.Exists("config.toml"))
            {
                Console.WriteLine("OselBot is a Telegram admin bot. It is needed to prohibit sending messages on behalf of the channel, as well as to karma users\nA token is necessary for the work of the bot. To get the token, you need to create a bot here: @BotFather\nThe mysql database is also required for work.");
                Console.Write("Please enter the token from the bot: ");
                string tokenBot = Console.ReadLine();
                Console.Write("\nSpecify the address to the database: ");
                string addressDatabase = Console.ReadLine();
                Console.Write("Specify the name of the database: ");
                string nameDatabase = Console.ReadLine();
                Console.Write("Now the username: ");
                string nameUserDatabase = Console.ReadLine();
                Console.Write("And the password: ");
                string passwordUserDatabase = Console.ReadLine();
                
                using (FileStream fstream = new FileStream("config.toml", FileMode.OpenOrCreate))
                {
                    string configText =
                            String.Format("botToken = '{0}'\naddressDatabase = '{1}'\nnameDatabase = '{2}'\nnameUserDatabase = '{3}'\npasswordUserDatabase = '{4}'",
                        tokenBot,
                        addressDatabase,
                        nameDatabase,
                        nameUserDatabase,
                        passwordUserDatabase);
                    Console.WriteLine(configText);
                    byte[] buffer = Encoding.Default.GetBytes(configText);
                    fstream.Write(buffer, 0, buffer.Length);
                }
            }
            using (FileStream fstream = File.OpenRead("config.toml"))
            {
                byte[] buffer = new byte[fstream.Length];
                fstream.Read(buffer, 0, buffer.Length);
                string textFromFile = Encoding.Default.GetString(buffer);

                var model = Toml.ToModel(textFromFile);
                bot = new TelegramBotClient((string) model["botToken"]!);;
            }

            MySqlConnection conn = DbUtils.GetDBConnection();
            DbUtils.CreateTableOrNo(conn);
            conn.Close();
            Console.WriteLine("Bot launched " + bot.GetMeAsync().Result.FirstName);

            var cts = new CancellationTokenSource();
            var cancellationToken = cts.Token;
            var receiverOptions = new ReceiverOptions
            {
                AllowedUpdates = { }, // receive all update types
            };
            bot.StartReceiving(
                HandleUpdateAsync,
                HandleErrorAsync,
                receiverOptions,
                cancellationToken
            );
            Console.ReadLine();
        }
    }
}
